﻿using System.Collections.Generic;
using UnityEngine.Analytics;
using System;

public class CustomEventTracker
{
    static public void SendTimeTookToCompleteLevelData(int levelNumber, float timeTook)
    {
        AnalyticsEvent.Custom("time_took_to_complete", new Dictionary<string, object>
        {
            {"Level", levelNumber},
            {"TimeTook", timeTook}
        });
    }

    static public void SendClickTimesData(int levelNumber, int clickTimes)
    {
        AnalyticsEvent.Custom("click_times", new Dictionary<string, object>
        {
            {"Level", levelNumber},
            {"ClickTimes", clickTimes}
        });
    }

    static public void SendSuccessRateData(int levelNumber, float successRate)
    {
        AnalyticsEvent.Custom("success_rate", new Dictionary<string, object>
        {
            {"Level", levelNumber},
            {"SuccessRate", successRate}
        });
    }

    static public void SendWrongChoiceTimes(int levelNumber, int wrongChoiceTimes)
    {
        AnalyticsEvent.Custom("wrong_choice_times", new Dictionary<string, object>
        {
            {"Level", levelNumber},
            {"WrongChoiceTimes", wrongChoiceTimes}
        });
    }

    static public void SendRightChoiceTimes(int levelNumber, int rightChoiceTimes)
    {
        AnalyticsEvent.Custom("right_choice_times", new Dictionary<string, object>
        {
            {"Level", levelNumber},
             {"RightChoiceTimes", rightChoiceTimes}
        });
    }
}
