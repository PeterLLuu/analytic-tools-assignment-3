﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    private GameManager m_manager;
    public GameManager.Symbols Symbol { get; private set; }
    private Transform m_child;
    private Collider m_collider;

    //Store first child to m_child
    //Store collider to m_collider
    private void Start()
    {
        m_child = this.transform.GetChild( 0 );
        m_collider = GetComponent<Collider>();
    }

    //Set TileSelected in GameManager to this card when being clicked
    private void OnMouseDown()
    {
        m_manager.TileSelected( this );
    }

    //Initialize m_manager to GameManager, and Symbol to symbol
    public void Initialize( GameManager manager, GameManager.Symbols symbol )
    {
        m_manager = manager;
        Symbol = symbol;
    }

    //Stop all running coroutines and start spinning 180 degrees, also disable collider so player cannot click on it
    public void Reveal()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 180, 0.8f ) );
        m_collider.enabled = false;
    }

    //Stop all running coroutines and spin back to hide card, also enabling collider so player can click on it
    public void Hide()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 0, 0.8f ) );
        m_collider.enabled = true;
    }

    //Spin target degrees in "time" seconds
    private IEnumerator Spin( float target, float time )
    {
        float timer = 0;
        float startingRotation = m_child.eulerAngles.y;
        Vector3 euler = m_child.eulerAngles;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            euler.y = Mathf.LerpAngle( startingRotation, target, timer / time );
            m_child.eulerAngles = euler;
            yield return null;
        }
        euler.y = target;
        m_child.eulerAngles = euler;
    }
}
