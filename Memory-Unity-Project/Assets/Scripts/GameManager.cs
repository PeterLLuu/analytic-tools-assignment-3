﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int CurrentLevelNumber;

    [System.Flags]
    public enum Symbols
    {
        Waves =             1 << 0,
        Dot =               1 << 1,
        Square =            1 << 2,
        LargeDiamond =      1 << 3,
        SmallDiamonds =     1 << 4,
        Command =           1 << 5,
        Bomb =              1 << 6,
        Sun =               1 << 7,
        Bones =             1 << 8,
        Drop =              1 << 9,
        Face =              1 << 10,
        Hand =              1 << 11,
        Flag =              1 << 12,
        Disk =              1 << 13,
        Candle =            1 << 14,
        Wheel =             1 << 15
    }

    [System.Serializable]
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    public GameObject TilePrefab;
    public float TileSpacing;

    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;

    private float levelTimeInSeconds;

    private int clickTimes;
    private float successRate;

    public int timesClickedWrong;
    public int timesClickedRight;



    private void SendEventTriggerOnLevelLoaded()
    {
        CustomEventTracker.SendTimeTookToCompleteLevelData(CurrentLevelNumber, levelTimeInSeconds);

        CustomEventTracker.SendClickTimesData(CurrentLevelNumber, clickTimes);


        successRate = (float)timesClickedRight / ((float)timesClickedRight + (float)timesClickedWrong);

        CustomEventTracker.SendSuccessRateData(CurrentLevelNumber, successRate);

        CustomEventTracker.SendWrongChoiceTimes(CurrentLevelNumber, timesClickedWrong);
        CustomEventTracker.SendRightChoiceTimes(CurrentLevelNumber, timesClickedRight);

        timesClickedWrong = 0;
        timesClickedRight = 0;
        levelTimeInSeconds = 0;
        clickTimes = 0;
    }

    private void CountLevelTime()
    {
        levelTimeInSeconds += Time.deltaTime;
    }

    private void Update()
    {
        CountLevelTime();

        if(Input.GetMouseButtonDown(0))
        {
            clickTimes++;
        }
    }


    private void Start()
    {
        LoadLevel( CurrentLevelNumber );
    }

    //Generate the level using levelNumber
    private void LoadLevel( int levelNumber )
    {
        

        //use levelNumber to calculate which "LevelDescription" is our current level
        LevelDescription level = Levels[levelNumber % Levels.Length];

        List<Symbols> symbols = GetRequiredSymbols( level );

        //for loop  through the rows and columns of current level and place tiles according to TileSpacing.
        //Randomly select a symbol and assign it to the generated tile
        for ( int rowIndex = 0; rowIndex < level.Rows; ++rowIndex )
        {
            float yPosition = rowIndex * ( 1 + TileSpacing );
            for ( int colIndex = 0; colIndex < level.Columns; ++colIndex )
            {
                float xPosition = colIndex * ( 1 + TileSpacing );
                GameObject tileObject = Instantiate( TilePrefab, new Vector3( xPosition, yPosition, 0 ), Quaternion.identity, this.transform );
                int symbolIndex = UnityEngine.Random.Range( 0, symbols.Count );
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol( symbols[symbolIndex] );
                tileObject.GetComponent<Tile>().Initialize( this, symbols[symbolIndex] );
                symbols.RemoveAt( symbolIndex );
            }
        }

        SetupCamera( level );
    }

    //Returns the required symbol base on the "level"
    private List<Symbols> GetRequiredSymbols( LevelDescription level )
    {
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        //fill symbols with symbols in allSymbols, throw argumentException if cardtotal is not even
        {
            Array allSymbols = Enum.GetValues( typeof( Symbols ) );

            m_cardsRemaining = cardTotal;

            if ( cardTotal % 2 > 0 )
            {
                new ArgumentException( "There must be an even number of cards" );
            }

            foreach ( Symbols symbol in allSymbols )
            {
                if ( ( level.UsedSymbols & symbol ) > 0 )
                {
                    symbols.Add( symbol );
                }
            }
        }

        //check if the symbols are 0 or any extra symbols for total cards
        {
            if ( symbols.Count == 0 )
            {
                new ArgumentException( "The level has no symbols set" );
            }
            if ( symbols.Count > cardTotal / 2 )
            {
                new ArgumentException( "There are too many symbols for the number of cards." );
            }
        }

        //check if needs to repeat and add the duplicated symbols to symbols
        {
            int repeatCount = ( cardTotal / 2 ) - symbols.Count;
            if ( repeatCount > 0 )
            {
                List<Symbols> symbolsCopy = new List<Symbols>( symbols );
                List<Symbols> duplicateSymbols = new List<Symbols>();
                for ( int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex )
                {
                    int randomIndex = UnityEngine.Random.Range( 0, symbolsCopy.Count );
                    duplicateSymbols.Add( symbolsCopy[randomIndex] );
                    symbolsCopy.RemoveAt( randomIndex );
                    if ( symbolsCopy.Count == 0 )
                    {
                        symbolsCopy.AddRange( symbols );
                    }
                }
                symbols.AddRange( duplicateSymbols );
            }
        }

        symbols.AddRange( symbols );

        return symbols;
    }

    //Find the position offset from symbol
    //x is determined by finding the remainder of symbolIndex and 4 and then divide that by 4
    //y is determined by dividing the symbolIndex by 16
    private Vector2 GetOffsetFromSymbol( Symbols symbol )
    {
        Array symbols = Enum.GetValues( typeof( Symbols ) );
        for ( int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex )
        {
            if ( ( Symbols )symbols.GetValue( symbolIndex ) == symbol )
            {
                return new Vector2( symbolIndex % 4, symbolIndex / 4 ) / 4f;
            }
        }
        Debug.Log( "Failed to find symbol" );
        return Vector2.zero;
    }

    //Setup the camera using level number
    //Set orthographicSize to the total size of level's rows and tilespacing so camera sees all elementss
    //Set camera position to the center of the level by getting the size of the level and divide that by 2
    private void SetupCamera( LevelDescription level )
    {
        Camera.main.orthographicSize = ( level.Rows + ( level.Rows + 1 ) * TileSpacing ) / 2;
        Camera.main.transform.position = new Vector3( ( level.Columns * ( 1 + TileSpacing ) ) / 2, ( level.Rows * ( 1 + TileSpacing ) ) / 2, -10 );
    }

    //Reveal tile if selected
    //hide symbol if matches
    //flip symbol back if doesnt match
    public void TileSelected( Tile tile )
    {
        if ( m_tileOne == null )
        {
            m_tileOne = tile;
            m_tileOne.Reveal();
        }
        else if ( m_tileTwo == null )
        {
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            if ( m_tileOne.Symbol == m_tileTwo.Symbol )
            {
                StartCoroutine( WaitForHide( true, 1f ) );
                timesClickedRight++;
            }
            else
            {
                StartCoroutine( WaitForHide( false, 1f ) );
                timesClickedWrong++;
            }
        }
    }

    //level number goes up by 1, if all levels completed then log GameOver, if not then load next level
    private void LevelComplete()
    {
        ++CurrentLevelNumber;
        SendEventTriggerOnLevelLoaded();
        if ( CurrentLevelNumber > Levels.Length - 1 )
        {
            Debug.Log( "GameOver" );
        }
        else
        {
            SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex );
        }
    }

    //A simple coroutine to hide cards in "time" seconds, and check if level completed
    private IEnumerator WaitForHide( bool match, float time )
    {
        float timer = 0;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            if ( timer >= time )
            {
                break;
            }
            yield return null;
        }
        if ( match )
        {
            Destroy( m_tileOne.gameObject );
            Destroy( m_tileTwo.gameObject );
            m_cardsRemaining -= 2;
        }
        else
        {
            m_tileOne.Hide();
            m_tileTwo.Hide();
        }
        m_tileOne = null;
        m_tileTwo = null;

        if ( m_cardsRemaining == 0 )
        {
            LevelComplete();
        }
    }
}
